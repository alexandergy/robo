const Gpio = require('pigpio').Gpio;

let logoutCounter = 0;
let lastDistance = 0;
let isOn = false;
let legs = {
  back: {
    left: {
      controller: new Gpio(13, {mode: Gpio.OUTPUT}),
      position: 0,
      increment: 5,
      start: 0
    },
    right: {
      controller: new Gpio(6, {mode: Gpio.OUTPUT}),
      position: 0,
      increment: 5,
      start: 0
    },
  },
  forward: {
    left: {
      controller: new Gpio(19, {mode: Gpio.OUTPUT}),
      position: 0,
      increment: 5,
      start: 0
    },
    right: {
      controller: new Gpio(26, {mode: Gpio.OUTPUT}),
      position: 0,
      increment: 5,
      start: 0
    },
  },
}

let detectors = {
  infrared: {
    controller: new Gpio(4, {
      mode: Gpio.INPUT,
      edge: Gpio.EITHER_EDGE,
    })
  }
}

// detectors.infrared.controller.on('interrupt', (value) => {
//   console.log('Controll value: ' + value);
//   if (value) {
//     walkForvard(true, true);
//   }
// })


function changeDirection(isRight, isForward) {
  logoutCounter++;
  if (logoutCounter >= 5) {
    console.log('Close, ' + isOn);
    isOn = false;
    logoutCounter = 0;
    return false;
  }
  console.log('Ongoing, ' + isOn);

  setTimeout(() => {
    if (isRight && isForward) {
      walkForvard(false, true);
    } else if (!isRight && isForward) {
      walkForvard(true, false);
    } else if (isRight && !isForward) {
      walkForvard(false, false);
    } else {
      walkForvard(true, true);
    }

  }, 1000);
}


function walkForvard(isRight, isForward) {
    let turn = isRight ? 'right' : 'left';
    let direction = isForward ? 'forward' : 'back';
    if (legs[direction][turn].position > 255) {
      legs[direction][turn].position = legs[direction][turn].start;
      changeDirection(isRight, isForward);
    } else {
      legs[direction][turn].controller.pwmWrite(legs[direction][turn].position);
      legs[direction][turn].position += legs[direction][turn].increment;
      setTimeout(() => {
        walkForvard(isRight, isForward);
      }, 20);
    }
}

var trigger = new Gpio(27, {mode: Gpio.OUTPUT});
var echo = new Gpio(17, {mode: Gpio.INPUT, alert: true});

// The number of microseconds it takes sound to travel 1cm at 20 degrees celcius
var MICROSECDONDS_PER_CM = 1e6/34321;

trigger.digitalWrite(0); // Make sure trigger is low

(function () {
var startTick;

echo.on('alert', function (level, tick) {
  var endTick,
    diff;

  if (level == 1) {
    startTick = tick;
  } else {
    endTick = tick;
    diff = (endTick >> 0) - (startTick >> 0);
    let distance = diff / 2 / MICROSECDONDS_PER_CM;

    if (distance > 50 && distance < 200 && !isOn & (Math.abs(distance - lastDistance) < 20)) {
      console.log('Triggered, ' + isOn + ': ' + diff / 2 / MICROSECDONDS_PER_CM);
      isOn = true;
      walkForvard(true, true);
    } else if (distance > 200) {
      console.log(diff / 2 / MICROSECDONDS_PER_CM);
    }
    lastDistance = distance;
  }
});
}());

// Trigger a distance measurement once per second
setInterval(function () {
trigger.trigger(10, 1); // Set trigger high for 10 microseconds
}, 1000);

// setInterval(function () {
//
//
//   dutyCycle += 5;
//   if (dutyCycle > 255) {
//     dutyCycle = 0;
//   }
// }, 20);
